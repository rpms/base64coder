%global long_ver  2010-12-19

Name:           base64coder
Version:        20101219
Release:        25%{?dist}
Summary:        Fast and compact Base64 encoder/decoder Java library
License:        EPL-1.0 or EPL-2.0 or LGPLv2+ or GPLv2+ or ASL 2.0 or BSD
BuildArch:      noarch
URL:            http://www.source-code.biz/%{name}/java/
Source0:        http://repo2.maven.org/maven2/biz/source_code/%{name}/%{long_ver}/%{name}-%{long_ver}-distribution.zip

BuildRequires:  maven-local
BuildRequires:  mvn(junit:junit)
BuildRequires:  mvn(org.apache.felix:maven-bundle-plugin)
BuildRequires:  mvn(org.apache.maven.plugins:maven-assembly-plugin)
BuildRequires:  mvn(org.apache.maven.plugins:maven-source-plugin)
BuildRequires:  mvn(org.sonatype.oss:oss-parent:pom:)

%description
Base64Coder is a fast and compact Base64 encoder/decoder class.

There is no Base64 encoder/decoder in the standard Java SDK class
library.  The undocumented classes sun.misc.BASE64Encoder and
sun.misc.BASE64Decoder should not be used.

%package javadoc
Summary:        API documentation for %{name}

%description javadoc
This package contains %{summary}.

%package tests
Summary:        Test jar for %{name}

%description tests
This package contains %{summary}.

%prep
%setup -q -n %{name}-%{long_ver}
sed -i 's/\r//g' README.txt CHANGES.txt
%mvn_file : %{name}
%pom_remove_plugin :maven-javadoc-plugin
%pom_xpath_inject pom:project/pom:build/pom:plugins '<plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-jar-plugin</artifactId>
        <version>3.1.2</version>
        <executions>
          <execution>
            <goals>
              <goal>test-jar</goal>
            </goals>
          </execution>
        </executions>
      </plugin>'
%mvn_package biz.source_code:base64coder:jar:tests:2010.12.19 tests
%mvn_artifact biz.source_code:base64coder:jar:tests:2010.12.19 target/%{name}-%{long_ver}-tests.jar

%build
%mvn_build

%install
%mvn_install

%files -f .mfiles
%doc README.txt CHANGES.txt

%files javadoc -f .mfiles-javadoc
%files tests -f .mfiles-tests

%changelog
* Thu Oct 03 2019 Severin Gehwolf <sgehwolf@redhat.com> - 20101219-25
- Add tests sub-package.

* Thu Oct 03 2019 Severin Gehwolf <sgehwolf@redhat.com> - 20101219-24
- Initial import from Fedora
